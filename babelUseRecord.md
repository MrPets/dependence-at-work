##### Babel 的配置文件是.babelrc，存放在项目的根目录下。使用 Babel 的第一步，就是配置这个文件。



~~~js
// 配置文件.babelrc
{
  "presets": [],
  "plugins": []
}
//presets字段设定转码规则，官方提供以下的规则集，你可以根据需要安装。
//# 最新转码规则
// npm install --save-dev @babel/preset-env

// # react 转码规则
//npm install --save-dev @babel/preset-react

//然后，将这些规则加入.babelrc。
{
	"presets": [
	  "@babel/env",
	  "@babel/preset-react"
	],
	"plugins": []
}

~~~

https://wangdoc.com/es6/intro.html#ecmascript-%E5%92%8C-javascript-%E7%9A%84%E5%85%B3%E7%B3%BB