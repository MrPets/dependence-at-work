/****************************************************设置cookie***************************************************/
function setCookie(name, value) {
	var Days = 30;
	var exp = new Date();
	exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}
function setCookie(name, value) {
    var Days = 1 / 3;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";path=/;domain=bozedu.net";
}
/****************************************************获取cookie****************************************************/
function getcookie(key) { //获取cookie方法
	/*获取cookie参数*/
	var getCookie = document.cookie.replace(/[]/g, ""); //获取cookie，并且将获得的cookie格式化，去掉空格字符
	var arrCookie = getCookie.split(";") //将获得的cookie以"分号"为标识 将cookie保存到arrCookie的数组中
	  // document.domain = 'bozedu.net';
	var tips; //声明变量tips
	for (var i = 0; i < arrCookie.length; i++) { //使用for循环查找cookie中的tips变量
	  var arr = arrCookie[i].trim().split("=");
    // console.log(arr) //将单条cookie用"等号"为标识，将单条cookie保存为arr数组
    if (key == arr[0]) { //匹配变量名称，其中arr[0]是指的cookie名称，如果该条变量为tips则执行判断语句中的赋值操作
      tips = arr[1]; //将cookie的值赋给变量tips
      break; //终止for循环遍历
    }
	}
	return tips;
}

/****************************************************逐级循环请求省市县组成一个json-这里的例子拼接到省市***************************************************/
function getOwnProCityAreaData() {
  //先获取一级数据(34个省、直辖市)
  var code = "";
  var parentcode = '';
  var listArray = [];
  var getAreaURL = "****************";//请求地址
  $.ajax({
      url: getAreaURL,
      data: {
        aa:'*****',
        bb: '*****',
        cc: '*****',
        dd:'',
        pid:0,//选填,地区id,筛选下级用,0=获取省级区域
      },
      async: false,
      type: "POST",
      dataType: "json", //数据类型为json
      success: function (data) {
        // console.dir(data);
        var secData = data.data.area;
          for (var i = 0; i < secData.length; i++) {
            var fenl = {} //定义一个空对象，作为每个一级数据对象
            fenl.text = secData[i].name//text: "北京市"
            fenl.value = secData[i].id//value: "110000",
            listArray.push(fenl)
            //这里的listArray是所有一级对象所组成的数组集合
            var parentcode = listArray[i].value//把一级数据的value值作为二级数据请求的入参
            //开始请求二级数据
            $.ajax({
              url: getAreaURL,
              data: {
                aa:'*****',
				        bb: '*****',
				        cc: '*****',
				        dd:'',
                pid:parentcode,//选填,地区id,筛选下级用,0=获取省级区域
               },
                async: false,
                type: "POST",
                dataType: "json", //数据类型为json
                success: function (data) {
                  var thrData = data.data.area;
                  listArray2 = [];
                  //这里很关键，每次请求下一个一级数据的子数据前，都要把上一个一级数据的子数据清空，要不然每个省都会拥有全国所有的市
                  for (var j = 0; j < thrData.length; j++) {
                    var fen2 = {} //定义一个空对象，作为每个二级数据对象
                    fen2.text = thrData[j].name
                    fen2.value = thrData[j].id
                    listArray2.push(fen2)
                    //这里的listArray是所有一级对象所组成的数组集合
                  }
                }
            })
            //最后将每个子数据对象所组成的数组集合插入到其对应的父级的children值中
            listArray[i].children = listArray2
          }
          //这里的listArray就是我们所需要的的json格式对象啦，功能和city.js文件一样！
          globalProCityArea=listArray;
      }
  })
}

/**************************************************筛选数组特定值为字符串-小功能****************************************************/
var dataLeft = [
  {"id": "001", "name": "梅超风"},
  {"id": "00121", "name": "梅靖"},
  {"id": "0013", "name": "张三丰"},
  {"id": "0014", "name": "令狐冲"},
  {"id": "0015", "name": "萧峰"},
  {"id": "0016", "name": "虚竹"}
];
// 首先var一个数组放你循环出来的数据
var nameArr = [];
dataLeft.forEach(function (item,index) {
    nameArr.push(item.name);
});
// nameArr的就是拼接后的新数组
console.log(nameArr);
// 通过数组的join方法拼接
var str = nameArr.join(",");
// str的就是拼接后的字符串
console.log(str); 


/**************************************************如何判断当前访问的是内网访问 还是外网****************************************************/
// 方法一：
var ispublic="";
// 请求成功，使用公网方式
httpRequest.onload = function () {
  // 请求完成。在此进行处理。
  if (httpRequest.readyState === 4 && httpRequest.status === 200) {
    param.isPublic = true
    that.initIMByType(param)
  }
}
// 响应超时，使用内网方式
httpRequest.ontimeout = function (e) {
  // XMLHttpRequest 超时。在此做某事。
  param.isPublic = false
  that.initIMByType(param)
}
// 方法二：
10.0.0.0/8：10.0.0.0～10.255.255.255
172.16.0.0/12：172.16.0.0～172.31.255.255
192.168.0.0/16：192.168.0.0～192.168.255.255
// 这个范围的是内网




/**************************************************Axios-POST网络通讯****************************************************/
 axios.post(url,data)
 .then(res=>{
	 console.log(res);
 })
 .catch(error=>{
	 console.log(error);
 })

 /**************************************************手机端判断初始化 localstorage的方法****************************************************/
function InitLocalstorage(){
  var userInfo=window.localstorage.getItem("userInfo");
  if(!userInfo){//如果本地没有localstorage
    var equipmentType=equipmentType();
    if(equipmentType!="ios" || equipmentType!="android"){//不是手机端设备,来自wap端
      //直接返回登录页面
      window.location.href="...."//登录页面
    }else{//设备是安卓或者是安卓(包含APP端 手机端的浏览器 pad端的浏览器)
      var client=GetQueryString("client");
      if(client=="ios" || client=="android"){//如果是app_ios或者是app_android传递过来的
        var token=getcookie("token");
        // 通过 token2user 换取用户信息
        let url=gobalData.userInfo.openapi+"/user/main/token2user";
        let params = {
            site: 'qqyxt',
            api: 'json',
            safekey: 'key',
            client: '',
            token: token
          };
        $.ajaxSettings.async = false;
        $.post(url, params, function(res) { 
          if(res.code==1){
            // window.localStorage.setItem('client','wx');
            window.localStorage.setItem('userInfo',JSON.stringify(res.data));
            var userInfo=JSON.parse(window.localStorage.getItem("userInfo"))
          }
        });
        // $.ajaxSettings.async = true; 
      }else if(client=="apad"){//来自平板端的wap端
        var usid=GetQueryString("usid");//从URL上截取usid;
        getuserInfo({
          url:'https://yzy.dev.bozedu.net',
          usid:usid
        })
      }else{//来自于手机端的浏览器
        //这里的操作是，有token直接用localstorage中，没有的话跳转登录页面
        var userInfo=window.localStorage.getItem("userInfo");
        if(!userInfo){
          window.location.href="....."//放置登录页
        }else{
          var token =JSON.parse(userInfo).token;
        }
      }
    }
    

  }

} 
 /**************************************************运行设备的判断****************************************************/
function equipmentType() {
  var u = navigator.userAgent;
  var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
  var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
  if (isAndroid) {
    return 'android'
  }
  if (isiOS) {
    return 'ios'
  }
},

//获取地址栏URL参数
function GetQueryString (name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]); return null;
}

//通过usid获取用户信息
function getuserInfo(data) {
  var params = {
      api: 'json',
      usid:data.usid
  }
  if(data.usid && data.url){
    $.ajax({
      url: data.url + '/index.php?mod=user&action=main&do=detail',
      type: 'post',
      async: true,
      data: params,
      success: function (res) {
        if (res.code == 1) {
          var userinfo=res.data;
          localStorage.setItem('userInfo',JSON.stringify(userinfo))
        }
      }
    })
  }else{
      alert('未获取到信息')
  }

} 












